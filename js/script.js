$(document).ready(function(){
	$('.btn-expand-collapse').click(function(e) {
		$('.navbar-primary').toggleClass('collapsed');
	});

	$(document).on("click",".item-primary",function(){
		$(".item-primary").removeClass('active');
		$(this).addClass('active');
		$(".main-content").html('');
		opcion=$(this).data('opc');
		
		if(opcion == 'principal')
			url="principal.php";
		if(opcion == 'galeria')
			url="galeria.php";

		$("#cargando").show();
		$.ajax({
			async:true,
			type:'post',
			dataType:'html',
			url:url,
			success:function(respuesta){
				$("#cargando").hide();
				$(".main-content").html(respuesta);
			},
			error:function(){
				alert("Ha ocurrido un error");
				$("#cargando").hide();
			}
		});
		return false;
	});

	$(document).on("click",".fila-nav-noticia",function(){
		$(".fila-nav-noticia").removeClass('active');
		$(this).addClass('active');
		titulo=$(this).data('titulo');
		contenido=$(this).data('contenido');
		url=$(this).data('imagen');
		imagen="<img src='"+url+"' class='prev-noticia'>";

		$(".marco-prev-noticia img").remove();
		$(".marco-prev-noticia").prepend(imagen);
		$(".titulo-prev-noticia").html(titulo);
		$(".contenido-prev-noticia").html(contenido);
	});

	$(document).on("click",".celda-thumb img",function(){
		$(".celda-thumb img").removeClass('active');
		$(this).addClass('active');
		titulo=$(this).data('titulo');
		url=$(this).data('url');
		$(".navegador-titulo").html(titulo);
		$(".visor-imagen").html("<img src='"+url+"'>");
	});

	$(document).on("click",".glyphicon-chevron-right",function(){
		thumbs=$(".img-thumb");
		cantidad=thumbs.length;

		activo=false;
		for(i=0; i<cantidad; i++){
			elemento=thumbs[i];
			if($(elemento).hasClass('active')){
				activo=i;
			}
		}

		if(activo < (cantidad-1)){
			siguiente=$(".celda-thumb img")[activo+1];
			$(siguiente).trigger('click');
		}
	});


	$(document).on("click",".glyphicon-chevron-left",function(){
		thumbs=$(".img-thumb");

		activo=false;
		for(i=0; i<cantidad; i++){
			elemento=thumbs[i];
			if($(elemento).hasClass('active')){
				activo=i;
			}
		}

		if(activo > 0){
			siguiente=$(".celda-thumb img")[activo-1];
			$(siguiente).trigger('click');
		}
	});

	$(".navbar-primary-menu li .active").trigger("click");
});

