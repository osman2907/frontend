<?php
$url ='http://jsonplaceholder.typicode.com/posts?userId=1';
$json = file_get_contents($url);
$posts = json_decode($json,true);
?>

<script type="text/javascript">
	$(document).ready(function(){
		primerPost=$(".fila-nav-noticia")[0];
		$(primerPost).trigger('click');
	});
</script>
<div class="principal">
	<div class="row">
		<div class="col-md-8">
			<div class="marco-prev-noticia">
				
				<div class="titulo-prev-noticia"></div>
			</div>
			<div class="contenido-prev-noticia"></div>
		</div>
		<div class="col-md-4">
			<?php 
			for($i=0; $i<4; $i++){ 
				$titulo=$posts[$i]['title'];
				$contenido=$posts[$i]['body'];
				if($i % 2 == 0)
					$imagen="images/noticias/noticias1.jpg";
				else
					$imagen="images/noticias/noticias2.jpg";
				?>
				<div class="row fila-nav-noticia" data-titulo="<?php echo $titulo; ?>" data-contenido="<?php echo $contenido; ?>" data-imagen="<?php echo $imagen; ?>">
					<div class="col-xs-6">
						<img src="<?php echo $imagen; ?>" class="prev-noticia">
					</div>
					<div class="col-xs-6 titulo-nav-noticia">
						<?php echo $titulo; ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>