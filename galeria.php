<?php
$url ='http://jsonplaceholder.typicode.com/photos?albumId=1';
$json = file_get_contents($url);
$fotos = json_decode($json,true);
?>

<script type="text/javascript">
	$(document).ready(function(){
		primeraImagen=$(".celda-thumb img")[0];
		$(primeraImagen).trigger('click');
	})
</script>

<div class="galeria">
	<div class="galeria2">
		<div class="row">
			<div class="col-xs-12 col-md-4 col-md-offset-4 visor-imagen"></div>
		</div>
		<div class="row navegador">
			<div class="col-xs-1 col-md-1 naveg-left"><span class="glyphicon glyphicon-chevron-left"></span></div>
			<div class="col-xs-8 col-md-10 navegador-titulo"></div>
			<div class="col-xs-1 col-md-1 naveg-right"><span class="glyphicon glyphicon-chevron-right"></span></div>
		</div>

		<div class="row thumbs">
			<?php 
			for($i=0; $i<11; $i++){
				$titulo=$fotos[$i]['title'];
				$thumb=$fotos[$i]['thumbnailUrl'];
				$url=$fotos[$i]['url'];
				?>
				<div class="col-xs-2 col-md-1 celda-thumb">
					<img src="<?php echo $thumb; ?>" class="img-thumb" data-titulo="<?php echo $titulo; ?>" data-url="<?php echo $url; ?>">
				</div>
				<?php
			}
			?>
		</div>	
	</div>

	
</div>