<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Evaluación Frontend</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">

	<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</head>
<body>
	<div id="cargando">
		<img src="images/loading.gif" title="Cargando" width="350px">
	</div>
	<nav class="navbar navbar-inverse navbar-global navbar-fixed-top">
      	<div class="container-fluid">
        	<div class="navbar-header">
          		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            		<span class="sr-only">Toggle navigation</span>
            		<span class="icon-bar"></span>
            		<span class="icon-bar"></span>
            		<span class="icon-bar"></span>
          		</button>
          		<a class="navbar-brand" href="#"></a>
        	</div>
        	<div id="navbar" class="collapse navbar-collapse">
        		<ul class="nav navbar-nav navbar-user navbar-right">
            		<li><a href="#"><span class="glyphicon glyphicon-bell icon-right"></span></a></li>
            		<li><a href="#"><span class="glyphicon glyphicon-comment icon-right"></span></a></li>
            		<li class="dropdown">
			          	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			          		Admin User 
			          		<img src="images/avatar.jpg" class="img-avatar">
			          		<span class="caret"></span>
			          	</a>
			          	<ul class="dropdown-menu">
			            	<li><a href="#">Perfil</a></li>
			            	<li><a href="#">Cerrar Sesión</a></li>
			          	</ul>
			        </li>
          		</ul>
        	</div><!--/.nav-collapse -->
      	</div>
    </nav>

	<nav class="navbar-primary">
  		<a href="#" class="btn-expand-collapse"><span class="glyphicon glyphicon-menu-left"></span></a>
  		<ul class="navbar-primary-menu">
	    	<li>
	    		<a href="#" class="item-primary active" data-opc="principal">
	    			<span class="glyphicon glyphicon-home"></span>
	    			<span class="nav-label">Página Principal</span>
	    		</a>
	      		<a href="#" class="item-primary" data-opc="galeria">
	      			<span class="glyphicon glyphicon-th-large"></span>
	      			<span class="nav-label">Galería</span>
	      		</a>
	    	</li>
  		</ul>
	</nav>

	<div class="main-content">
		
	</div>


	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>